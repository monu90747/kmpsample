class SignupViewModel: SignupViewModelProtocol {
    var showErrorMessage: Bindable<String?> = Bindable(nil)
    var onSuccess: Bindable<Bool> = Bindable(false)

    func submit(name: String, email: String, password: String, phone: String) {}
}
