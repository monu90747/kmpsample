//
//  SignupViewController.swift
//  SampleiOSApp
//
//  Created by Monu Rathor on 25/02/22.
//  Copyright © 2022 orgName. All rights reserved.
//

import UIKit

class SignupViewController: UIViewController {
    @IBOutlet private weak var fullNameTextField: UITextField!
    @IBOutlet private weak var emailTextField: UITextField!
    @IBOutlet private weak var passwordTextField: UITextField!
    @IBOutlet private weak var phoneNumberTextField: UITextField!

    var viewModel: SignupViewModelProtocol!

    override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModel()
    }

    private func bindViewModel() {
        viewModel.onSuccess.bind { success in
            guard success else { return }
        }

        viewModel.showErrorMessage.bind { [weak self] message in
            guard let message = message else { return }
            self?.showAlert(with: message)
        }
    }

    @IBAction private func submitTapAction(_ sender: Any) {
        viewModel.submit(
            name: fullNameTextField.trimText,
            email: emailTextField.trimText,
            password: passwordTextField.trimText,
            phone: phoneNumberTextField.trimText
        )
    }
}

extension SignupViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case fullNameTextField:
            emailTextField.becomeFirstResponder()
        case emailTextField:
            passwordTextField.becomeFirstResponder()
        case passwordTextField:
            phoneNumberTextField.becomeFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return true
    }
}
