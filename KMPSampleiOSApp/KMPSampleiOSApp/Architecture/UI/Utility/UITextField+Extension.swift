//
//  UITextField+Extension.swift
//  SampleiOSApp
//
//  Created by Monu Rathor on 25/02/22.
//  Copyright © 2022 orgName. All rights reserved.
//

import UIKit

extension UITextField {
    var trimText: String {
        return text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) ?? ""
    }
}
