//
//  UIViewController+Alert.swift
//  SampleiOSApp
//
//  Created by Monu Rathor on 25/02/22.
//  Copyright © 2022 orgName. All rights reserved.
//

import UIKit

extension UIViewController {
    func showAlert(with message: String) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.addAction(.init(title: "OK", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
