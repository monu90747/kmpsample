//
//  AppCoordinator.swift
//  SampleiOSApp
//
//  Created by Monu Rathor on 25/02/22.
//  Copyright © 2022 orgName. All rights reserved.
//

import UIKit

class AppCoordinator: BaseCoordinator {
    override func start() {
        let storyboard = UIStoryboard(name: "Authentication", bundle: nil)
        guard let controller = storyboard.instantiateInitialViewController() as? SignupViewController else { return }
        controller.viewModel = SignupViewModel()
        navigationController.viewControllers = [controller]
    }
}
