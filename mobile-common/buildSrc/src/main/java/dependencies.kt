const val mockito_version = "3.7.7"
const val mockitoKotlin_version = "2.2.0"
const val kotlinx_coroutines = "1.5.2-native-mt"
const val kotlinx_coroutines_common = "1.3.8"
const val kotlinx_date_time = "0.2.1"

const val kotlinxCoroutines = "org.jetbrains.kotlinx:kotlinx-coroutines-core:$kotlinx_coroutines"
const val kotlinxCoroutinesCommon = "org.jetbrains.kotlinx:kotlinx-coroutines-core-common:$kotlinx_coroutines_common"
const val kotlinxDateTime = "org.jetbrains.kotlinx:kotlinx-datetime:$kotlinx_date_time"
